#!/usr/bin/env bash

curl-api() {
    TOKEN=$(gopass show www/gitlab.vgiscience.de/ml/access_tokens/sudo_api)
    URL="https://gitlab.vgiscience.de/api/v4"
    curl --silent --request "$1" --header "PRIVATE-TOKEN: $TOKEN" "$URL/$2?$3"
}

yq eval -o j '.project_ids, .yrg_ids' _config.yml |
    jq -r '.[]' |
    while IFS= read -r pid; do
        curl-api GET projects/"$pid" | jq
    done
