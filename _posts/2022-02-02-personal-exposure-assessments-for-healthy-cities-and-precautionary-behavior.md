---
layout: post
title: "Personal exposure assessments for healthy cities and precautionary behavior"
date: 2022-02-02 10:22:13+01:00
author: Uwe Schlink
---

About 300 volunteers in Leipzig have gathered data on personal exposure to urban environmental parameters as Volunteered Geographic Information (VGI) during the past two years. Persons wearing environmental sensors that register air quality, heat, noise, and geolocation have a high potential to assess personal health risks as well as the spatiotemporal distribution of urban environmental conditions.

The [VGIscience project ExpoAware](/projects/expoaware.html), jointly performed by psychologists of the University of Leipzig and urban researchers of the Helmholtz Centre for Environmental Research, studies the exposure data and movement patterns and activity spaces of the participating volunteers. The attached video provides an overview of the aims and methods of this project.

<video width="100%" poster="/images/2022-02-02-personal-exposure-assessments-for-healthy-cities-and-precautionary-behavior.jpg" controls>
  <source src="https://downloads.vgiscience.org/projects/expoaware/2022-02-02-personal-exposure-assessments-for-healthy-cities-and-precautionary-behavior.webm" type="video/webm">
  <source src="https://downloads.vgiscience.org/projects/expoaware/2022-02-02-personal-exposure-assessments-for-healthy-cities-and-precautionary-behavior.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

As an additional subject, the researchers will now investigate the effect of feedback on the opinion, motivation, and behavior of the volunteers. Will they mitigate their exposure and avoid hot spots of pollution and heat in the future? For that purpose the volunteers are informed by feedback tailored to them, representing their personal measurements and displayed on their smartphone.

The study is further extended by an automated recommendation of less polluted routes created by an algorithm that is developed in the [VGI-routing project at the University of Bonn](/projects/vgi-routing.html). This cooperation is an advantage of the VGIscience research program, which is very conducive to joint research activities between specialists from different disciplines.
