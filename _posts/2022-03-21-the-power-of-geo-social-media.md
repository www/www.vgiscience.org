---
layout: post
title: "The Power of Geo-Social Media"
date: 2022-03-21 14:15:27+01:00
author: Dirk Burghardt
---

Two projects from the VGIscience priority program are invited to the IJGI Webinar "The power of Geo-Social Media":

Dr. Eva Hauthal ([EVA-VGI](/projects/eva-vgi.html)) will present research related to "Emojis as Contextual Indicants in Location-Based Social Media Posts".

Martin Knura ([TOVIP](/projects/tovip.html)) will give a presentation "Using Object Detection on Social Media Images for Urban Bicycle Infrastructure Planning: A Case Study of Dresden".

Date: 4 April 2022  
Time: 5:00pm CEST | 11:00am EDT | 11:00pm CST Asia  
Event registration: [ijgi-3.sciforum.net](https://ijgi-3.sciforum.net)
