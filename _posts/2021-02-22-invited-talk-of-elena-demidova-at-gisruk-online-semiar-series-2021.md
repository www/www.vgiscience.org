---
layout: post
title: "Invited talk of Elena Demidova at GISRUK Online Semiar Series 2021"
date: 2021-02-22 15:58:02+01:00
author: Dirk Burghardt, TU Dresden
---

*Thursday 25th March 2021 at 14:00 UTC (15:00 CET)*

*Semantic geographic knowledge on a world-scale –*
*interlinking OpenStreetMap and knowledge graphs*

Prof. Dr. Elena Demidova is Professor of Computer Science at the University of
Bonn, Germany, where she leads Data Science & Intelligent Systems research group
(DSIS). In the past, Elena worked as Research Group Leader at the L3S Research
Center, Leibniz University of Hannover, Germany, and as Senior Research Fellow
at the Web and Internet Science Group at the University of Southampton, UK. She
received her PhD degree from the Leibniz University of Hannover in 2013 and her
MSc degree in Information Engineering in 2006. Elena’s main research interests
are Data Analytics, Open Data, the Web, and the Semantic Web.

OpenStreetMap (OSM) is a rich source of openly available volunteered geographic
information on a world scale. However, representations of geographic entities in
OSM are highly diverse and incomplete. Knowledge graphs (i.e. graph-based
knowledge repositories) such as Wikidata, EventKG, and DBpedia are a rich source
of contextual semantic information about geographic entities. For example,
Wikidata contains over six million geographic entities, including locations,
points of interest, mountain peaks, etc. Whereas knowledge graphs provide a wide
range of complementary semantic information for geographic entities,
interlinking between knowledge graphs and OSM is insufficient with the links
mainly manually defined by volunteers. This lecture will introduce emerging
approaches that address tighter integration of OSM and knowledge graphs; it
gives particular attention to link discovery and semantic enrichment of OSM
datasets.

**How to participate:** This event is an online event and is completely
free-of-charge to attend. However, registration is necessary. A registration
form is available [here on Eventbrite][1]. Registration closes on Wednesday 24
th March 2021 at 23:59 UTC.

**More information:** Event Chair Peter Mooney ( peter.mooney@mu.ie ), Twitter
@GISRUK Hashtag #GISRUKLectures


[1]: https://www.eventbrite.co.uk/e/gisruk-online-seminar-series-2021-seminar-1-tickets-141817862043?aff=erelexpmlt
