---
layout: post
title: "VGIscience Springer book published open access"
date: 2023-12-12 11:33:17+01:00
author: Dirk Burghardt
---

![](/images/2023-12-12-vgiscience-springer-book-published-open-access.jpg)

We are happy to announce the release of our book!

This open access book includes methods for retrieval, semantic representation, and analysis of Volunteered Geographic Information (VGI), geovisualization and user interactions related to VGI, and discusses selected topics in active participation, social context, and privacy awareness. It presents the results of the DFG-funded priority program "VGI: Interpretation, Visualization, and Social Computing" (2016-2023).

The book includes three parts representing the principal research pillars within the program. Part I "Representation and Analysis of VGI" discusses recent approaches to enhance the representation and analysis of VGI. It includes semantic representation of VGI data in knowledge graphs; machine-learning approaches to VGI mining, completion, and enrichment as well as to the improvement of data quality and fitness for purpose. Part II "Geovisualization and User Interactions related to VGI" book explores geovisualizations and user interactions supporting the analysis and presentation of VGI data. When designing these visualizations and user interactions, the specific properties of VGI data, the knowledge and abilities of different target users, and technical viability of solutions need to be considered. Part III "Active Participation, Social Context and Privacy Awareness" of the book addresses the human impact associated with VGI. It includes chapters on the use of wearable sensors worn by volunteers to record their exposure to environmental stressors on their daily journeys, on the collective behavior of people using location-based social media and movement data from football matches, and on the motivation of volunteers who provide important support in information gathering, filtering and analysis of social media in disaster situations.

The book is of interest to researchers and advanced professionals in geoinformation, cartography, visual analytics, data science and machine learning. It is published at [Springer Professional][1] under [Creative Commons Attribution 4.0 International License (CC BY 4.0)][2].

```
Title:
  - Volunteered Geographic Information

Editors:
  - Dirk Burghardt
  - Elena Demidova
  - Daniel A. Keim

Copyright Year:
  - 2024

Publisher:
  - Springer Nature Switzerland

Electronic ISBN:
  - 978-3-031-35374-1

Print ISBN:
  - 978-3-031-35373-4

DOI:
  - https://doi.org/10.1007/978-3-031-35374-1
```

[1]: https://www.springerprofessional.de/en/volunteered-geographic-information/26509080
[2]: https://www.creativecommons.org/licenses/by/4.0/

<style>
    .post-content img {
        width: 35%;
        float: right;
        padding: 0 0 1em 1em;
    }
</style>
