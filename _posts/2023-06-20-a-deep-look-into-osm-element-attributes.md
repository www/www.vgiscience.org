---
layout: post
title: "A Deep Look into OSM Element Attributes"
date: 2023-06-20 17:00
author:
    - Moritz Schott (GIScience Research Group of the Institute of Geography at Heidelberg University)
    - Prof. Dr. Sven Lautenbach (HeiGIT gGmbH)
---


Many of the VGIScience SPP phase 2 projects have been completed or are nearing their end, and so is the [IDEAL-VGI](https://www.vgiscience.org/projects/ideal-vgi.html) project. And while that won’t be the end of our research into social data sources, data quality analyses and land-use information, we are happy to announce a major outcome: the version 1 release of the [OSM Element Vectorisation Tool](https://gitlab.gistools.geog.uni-heidelberg.de/giscience/ideal-vgi/osm-element-vectorisation) (OEV). The tool was first presented to the public at [FOSS4G 2022](https://giscienceblog.uni-heidelberg.de/2023/02/03/high-resolution-data-insights-from-openstreetmap-element-vectorisation/) in Florence but has since received multiple updates and enhancements.

The release is accompanied by a video podcast featuring Prof. Dr. Sven Lautenbach and Moritz Schott. In the video, the researchers provide an introduction to OSM, OSM data quality and the OEV.

[![](https://www.geog.uni-heidelberg.de/md/chemgeo/geog/gis/vgiscience_heidelberg_interview_thumbnail.jpg)](https://www.geog.uni-heidelberg.de/md/chemgeo/geog/gis/vgiscience_heidelberg.mp4)

The [OSM Element Vectorisation Tool](https://gitlab.gistools.geog.uni-heidelberg.de/giscience/ideal-vgi/osm-element-vectorisation) (OEV) enables high resolution data insights into the OSM database.  Through researchers, corporations, and (mainly) private users and contributors, the OSM ecosystem has grown to encompass a massive set of tools and services that accompany the data and maps themselves. However, many of these tools and services are either general purpose tools to “do something with the data” or specialized tools to “find out a specific thing” rather than a combination of the two. For example, [is-osm-uptodate](https://is-osm-uptodate.frafra.eu/) is a fantastic tool to answer the exact question in its name in a data-driven manner. Alternatively, scientific studies often concentrate on a specific region, as seen in [comparing OSM to official data in Teheran (Iran)](https://www.mdpi.com/2220-9964/3/2/750).

The tool sets out to overcome these limitations by combining and providing what has so far been divided and conquered. Specifically, the tool creates a multi-faceted data view that is neither limited to a certain region nor to a specific topic. The goal is to enable data enthusiasts, data analysts and machine learners to visualize a multi-dimensional view of the data without the setup of requisite tools, data and services.

![](https://gitlab.gistools.geog.uni-heidelberg.de/uploads/-/system/project/avatar/565/osm-element-vectorisation.png)

OEV computes more than 37 data dimensions and is based on more than 16 scientific studies, with more dimensions and studies planned for future incorporation. Since the [presentation of the tool at the FOSS4G conference 2022 in Florence](https://talks.osgeo.org/foss4g-2022-academic-track/talk/MCGBBT/), additional data insights have been added including user specialization and diversity. These two aspects provide information about the professionalism of the objects editors by calculating how experienced the users are with edits on objects of this type (e.g., land-use, buildings or shops). Moreover, the tool has been made more robust and versatile and is now ready for a first official release to the public.

**The [full list of indicators and their documentation is available in the tools repository](https://gitlab.gistools.geog.uni-heidelberg.de/giscience/ideal-vgi/osm-element-vectorisation/-/blob/main/doc/indicators.md). But you can also quickly dive into its usage on our intuitive [frontend](https://oev.geog.uni-heidelberg.de/) which provides some precomputed examples.**

While we use the tool in our current research and have shown its practicality in the [article](https://isprs-archives.copernicus.org/articles/XLVIII-4-W1-2022/395/2022/) “OpenStreetMap Element Vectorization- A Tool for High Resolution Data Insights and Its Usability in the Land-Use Land-Cover Domain” published in The International Archives of the Photogrammetry, Remote Sensing and Spatial Information Sciences, we have added additional examples in a dedicated repository: [OEV Examples](https://gitlab.gistools.geog.uni-heidelberg.de/giscience/ideal-vgi/oev_examples).


## Related work

Bruckner, J., Schott, M., Zipf, A., Lautenbach, S., 2021. Assessing shop completeness in OpenStreetMap for two federal states in Germany. AGILE: GIScience Series, 2, 20.

Herfort, B., Lautenbach, S., de Albuquerque, J. P., Anderson, J., Zipf, A., 2021. The evolution of humanitarian mapping within the OpenStreetMap community. Scientific Reports, 11(1), 1–15.

Jokar Arsanjani, J., Mooney, P., Zipf, A., Schauss, A., 2015. Quality assessment of the contributed land use information from openstreetmap versus authoritative datasets. J. Jokar Arsanjani, A. Zipf, P. Mooney, M. Helbich (eds), OpenStreetMap in GIScience: Experiences, Research, and Applications, Springer International Publishing, Cham, 37–58.

Neis, P., Zielstra, D., Zipf, A., 2013. Comparison of Volunteered Geographic Information Data Contributions and Community Development for Selected World Regions. Future Internet, 5(2), 282–300.

Neis, P., Zipf, A., 2012. Analyzing the Contributor Activity of a Volunteered Geographic Information Project — The Case of OpenStreetMap. ISPRS International Journal of GeoInformation, 1(2), 146–165.

Raifer, M., Troilo, R., Kowatsch, F., Auer, M., Loos, L., Marx, S., Przybill, K., Fendrich, S., Mocnik, F.-B., Zipf, A., 2019. OSHDB: a framework for spatio-temporal analysis of OpenStreetMap history data. Open Geospatial Data, Software and Standards, 4(1), 1–12.

Schott, M., Grinberger, A. Y., Lautenbach, S., Zipf, A., 2021. The Impact of Community Happenings in OpenStreetMap—Establishing a Framework for Online Community Member Activity Analyses. ISPRS International Journal of GeoInformation, 10(3), 164.

[Schott, M., Lautenbach, S., Größchen, L., and Zipf, A.: OpenStreetMap Element Vectorisation A Tool For High Resolution Data Insights And Its Usability In The Land Use And Land Cover Domain, Int. Arch. Photogramm. Remote Sens. Spatial Inf. Sci., XLVIII-4/W1-2022, 395–402, 2022.](https://doi.org/10.5194/isprs-archives-XLVIII-4-W1-2022-395-2022)

Schott, M., Zell, A., Lautenbach, S., Zipf, A., Demir, B., 2022. LULC multi-tags based on OSM, Version 0.1. [https://gitlab.gistools.geog.uni-heidelberg.de/giscience/idealvgi/osm-multitag](https://gitlab.gistools.geog.uni-heidelberg.de/giscience/idealvgi/osm-multitag).

Schultz, M., Voss, J., Auer, M., Carter, S., Zipf, A., 2017. Open land cover from OpenStreetMap and remote sensing. International Journal of Applied Earth Observation and Geoinformation, 63, 206-213.

Zielstra, D., Zipf, A., 2010. A comparative study of proprietary geodata and volunteered geographic information for Germany. 13th AGILE international conference on geographic information science, 2010, 1–15.
