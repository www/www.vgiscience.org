---
layout: post
title: "Environmental pollution in city traffic"
date: "2020-10-12 07:11:35 +0100"
author:
    - Uwe Schlink (UFZ Leipzig)
---

**How strong are Leipzigs cyclists and pedestrians affected by
environmental pollution in city traffic?**

![](/images/2020-10-12-environmental-pollution-in-city-traffic-1.jpg)
*Figure 1: Participant wearing the mobile sensors*

Cities worldwide suffer of increasing temperatures, air pollutants, and
noise. Such urban environmental factors are only registered at a few
measuring stations. We want to know where and when the people of Leipzig
are exposed to harmful environmental factors in their everyday life: Are
there multiple exposures, e.g. heat, particulate matter, and noise at
the same time? Which people are particularly affected? How do cyclists
and pedestrians perceive air, noise, and thermal stress in summer and
how does this agree with the measured data, especially during heat
waves?

The DGF project ExpoAware started in February 2020 and aims to
investigate exposure of individuals to environmental pollutants on their
daily ways through the city as well as to explore the behavior and
decision motives of individuals during everyday mobility. Despite
diverse covid-19 restrictions in public life we managed to receive more
than 300 registrations for our study in 2020 and started in July 2020.
The participants have been equipped with a set of mobile sensors (Fig.
1) consisting of

a) Smartphone for GPS, acceleration, light, and noise  
b) Gas sensor for NO2, NO, CO, O3, as well as temperature and humidity
    controlled via smartphone app  
c) Particle tracker counting number of small and large particles  

With the help of a smartphone app specially developed for the study, we
also collected data about the kind of way (Fig. 2). About 20
participants were assigned for each week and were randomly grouped into
a measurement group and a control group. Around 48% of those registered
took part in the measurement and completed the questionnaires. In the
next steps we will analyze the environmental data using methods of
scientific visualization, visual analytics and pattern recognition as
well as combine it with the subjective data from the questionnaires. In
addition, we will test the effects of wearing a mobile sensor and
receiving feedback about personal exposure on psychological variables
such as threat appraisals and different forms of coping e.g. intentions
to change mobility behavior.

![](/images/2020-10-12-environmental-pollution-in-city-traffic-2.jpg)
*Figure 2: Prepared devices at the issuing office at Leipzig University*
