---
layout: post
title: "VGIScience at SotM and FOSS4G 2022"
date: 2022-08-15 17:12:26+02:00
author: Moritz Schott
---

We are happy to announce that this year two VGIScience projects will be actively
contributing to the [FOSS4G](https://2022.foss4g.org/) and the [State of the Map
(SotM)](https://2022.stateofthemap.org/) conferences in Florence. FOSS4G is a
large community meeting of developers, users, companies and scientists that use
and develop free and open source software (FOSS). It is hosted by the OSGeo
foundation, a leading distributor of FOSS geo-software. SotM is the leading
meeting for the OpenStreetMap community where data users, mappers, developers
and scientists come together to discuss current trends and innovations. The
schedule is extremely packed for both of them so we want to highlight some
topics you should definitely add to your schedule. Online and offline
registration is still open and we would be happy to meet you in Italy or online:


### SotM2022

  * *Su, 21.08.2022, 10:00:* In their talk [Comparative Integration Potential
    Analyses of OSM and Wikidata – the Case Study of Railway
    Stations](https://2022.stateofthemap.org/sessions/YU9JHN/), Alishiba Dsouza
    (University Bonn, DSIS , WoldKG Project) and Moritz Schott (Heidelberg
    University, GIScience, IDEAL-VGI Project) calculated dataset and community
    metrics to compare these two great community projects
  * *Su, 21.08.2022, 10:50:* In the five minute lightning talk [Returning the
    favor - Leveraging quality insights of OpenStreetMap-based
    land-use/land-cover multi-label modeling to the
    community](https://2022.stateofthemap.org/sessions/EKEZ7R/) you will learn
    about how OSM was used in a deep learning based approach for remote sensing
    image classification and how this could (and in future 'should', by default)
    benefit the OSM community. The topic is a result of the IDEAL-VGI project
    and a collaboration between Moritz Schott, Sven Lautenbach and Alexander
    Zipf from GIScience/HeiGIT at Heidelberg University and Adina Zell and Begüm
    Demir from RSiM research group at TU Berlin.

### FOSS4G

  * *Th, 25.08.2022, 14:15:* The new tool [OSM Element
    Vectorisation](https://talks.osgeo.org/foss4g-2022-academic-track/talk/MCGBBT/)
    that enables users to calculate a large set of quality indicators for single
    OSM elements is presented. The tool is an outcome of the IDEAL-VGI project.
    In an example study it was used to uncover some interesting tendencies in
    OSMs land-use data.
