---
layout: post
title: "Lecture Series: Privacy Aspects in VGI"
date: 2021-07-22 15:14:26+02:00
author: Marc Löchner
---

As part of the [VGIscience 2021 lecture series](/lecture-series.html), this weeks lecture from Marc Löchner from TU Dresden was about [privacy aspects in volunteered geograpic information](/projects/privacy-aspects.html) (VGI).

Emphasizing why privacy aspects are cruicial in this field, the lecture covers research results from the entire priority programme, including the first phase. This includes publications about Abstraction Layers, HyperLogLog as a technique to store location-based social media data, extended explorations of opportunities with advanced set operations, wrapped up with a case study about a real-life use case in disaster management.

<video width="100%" controls>
  <source src="https://downloads.vgiscience.org/lecture-series/privacy-aspects/privacy-aspects.webm" type="video/webm">
  <source src="https://downloads.vgiscience.org/lecture-series/privacy-aspects/privacy-aspects.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
