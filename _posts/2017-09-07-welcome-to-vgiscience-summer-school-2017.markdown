---
layout: post
title: "Welcome to VGIscience Summer School 2017"
date: "2017-09-07 12:21:39 +0200"
---

The priority programme VGIscience of the German Research Foundation will host a summer school dedicated to Interpretation, Visualisation and Social Computing of Volunteered Geographic Information (VGI). The summer school will be held from 11.-15. September 2017 at TU Dresden, Germany.

The aim of the summer school is to give PhD students who are at an early stage of their academic career insights in to current research topics related to geographic information retrieval, geovisual analytics and social science and the application of such methods to VGI. Within this context a selection of methods will be presented more in detail, including graphical probabilistic models, deep learning, space and time extraction, interaction and coordinated views as well as quantitative and qualitative methods in social science. The summer school will include keynotes and lectures, demonstrations and guided practical exercises as well as project related independent group work. The working language of the summer school will be English.

Get all the details [on our dedicated VGIscience Summer School website](https://summerschool.vgiscience.org).

See you next week!
