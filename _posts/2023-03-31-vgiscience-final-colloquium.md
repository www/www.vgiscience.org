---
layout: post
title: "VGIscience final colloquium"
date: 2023-03-31 15:00:00+02:00
author: Dirk Burghardt
---

The final event of the DFG priority program VGIscience took place at 
HafenCity University on 2./3. March 2023 with all research groups 
involved, including invited reviewers.

The following topics were presented and discussed over the two days:

* Review of 6 years of VGIscience, Prof. Burghardt
  ([slides](https://downloads.vgiscience.org/conferences/2023-03-03_VGIscience_Final_Colloquium_Hamburg/VGIscience_Overview_Burghardt.pdf))
* Keynote Prof. Werner Kuhn: “Interdisciplinarity quo vadis: What can we learn from the VGI experience?”
* Presentation of the research results of the [individual projects](https://www.vgiscience.org/2nd-phase-projects.html)
* Selected SPP collaboration projects of the [Young Research Groups](https://www.vgiscience.org/yrg.html)
* Panel future of VGI research

![VGIscience final colloquium participants](/images/2023-03-31-vgiscience-final-colloquium.jpg)
*The photo shows the participants of the VGIscience final colloquium in HafenCity University Hamburg.*
