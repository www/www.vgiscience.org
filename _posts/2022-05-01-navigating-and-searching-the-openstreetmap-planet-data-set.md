---
layout: post
title: "Navigating and Searching the OpenStreetMap Planet Data Set"
date: 2022-05-01 09:49:46+02:00
author: Stefan Funke
---

In recent years, there has been a dramatic growth of geospatial data collected by companies like Apple, Google, or Here but also in the course of collaborative projects like OpenStreetMap (OSM). For example, while the OSM data set at the beginning in 2007 contained less than 30 million data items, by the end of 2022, this number has grown to more than 7 billion items contributed mainly by volunteers all over the world. 

There are many challenges when dealing with such a non-tiny data set. For example, computing the optimal route in a road network with a size of several hundred millions of road segments immediately excludes standard algorithms like Dijkstra which would require several minutes of computation for a single query. Furthermore, efficiently querying and retrieving data from this data set is quite different from querying a small company employee database. Asking for all bank branches world-wide not only poses a challenge when determining the actual result of such a query, but also in terms of an efficient and comprehensible representation and visualization of the result set.

The [Trajectories project][p] funded during the the two phases of the SPP has developed algorithmic techniques and tools that allow for the efficient processing of trajectory data in the context of the OpenStreetMap project, which itself is partially created from trajectory data (the road network). As part of these research efforts in the SPP, [the search engine OSCAR][s] has been developed which allows for non-trivial querying and routing on the complete OSM planet data set. 

[Figure 1][1], for example, shows the result of querying OSCAR for all train stations world-wide. The result is presented as a heat map visualization including statistics about the distribution over all countries. Querying our search engine for restaurants in San Francisco (CA) yields the result shown in [Figure 2][2] with an automatic differentiation of the respective restaurants according to their cuisine.
OSCAR also allows for highly efficient route planning: a trip of more than 13,000 km from Portugal to Hong Kong is computed within a fraction of a second and hotels along the route are categorized according to their star rating (Google Maps can't do that!), see [Figure 3][3].  

[We have prepared a short video][v] hat gives an impression about the power of OSCAR, but feel free to experiment yourself with [our search engine][s].

### Figures

[![Figure 1](/images/2022-05-01-navigating-and-searching-the-openstreetmap-planet-data-set_thumb1.png)][1]
[![Figure 2](/images/2022-05-01-navigating-and-searching-the-openstreetmap-planet-data-set_thumb2.png)][2]
[![Figure 3](/images/2022-05-01-navigating-and-searching-the-openstreetmap-planet-data-set_thumb3.png)][3]


<style>
    img[src*="thumb"] {
        width: auto;
        height: 192px;
        padding: 0 1em 1em 0;
    }
</style>

[1]: /images/2022-05-01-navigating-and-searching-the-openstreetmap-planet-data-set_fig1.png
[2]: /images/2022-05-01-navigating-and-searching-the-openstreetmap-planet-data-set_fig2.png
[3]: /images/2022-05-01-navigating-and-searching-the-openstreetmap-planet-data-set_fig3.png
[p]: /projects/trajectories-2.html
[s]: https://www.oscar-web.de
[v]: https://downloads.vgiscience.org/projects/trajectories/2022-05-01-navigating-and-searching-the-openstreetmap-planet-data-set.html
