---
layout: post
title: "Best paper award at EuroVA"
date: "2018-06-05 15:41:58 +0200"
author: Gennady Andrienko
---

The SPP VGIscience paper [Visual Exploration of Spatial and Temporal Variations of Tweet Topic Popularity][1] by Jie Li, Siming Chen, Gennady Andrienko and Natalia Andrienko received the best paper award at EuroVA, a workshop at [EuroVis][2].

Many thanks to Jie and Siming!



[1]: https://diglib.eg.org/handle/10.2312/eurova20181105
[2]: http://www.eurova.org
