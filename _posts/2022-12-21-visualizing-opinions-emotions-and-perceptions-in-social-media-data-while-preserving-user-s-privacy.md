---
layout: post
title: "Visualizing opinions, emotions and perceptions in social media data - while preserving user privacy"
date: 2022-12-21 12:33:21+01:00
author: Marc Löchner
---

[EVA-VGI] and the [Privacy Aspects] project worked together during the second phase of the VGIscience priority programme to apply theoretical research results on user privacy to practical visual analytics examples. The background of this research and the motivation of participating colleagues is shown in a video, which we are happy to be able to release to the public today.

The film features three of the researchers from the Institute of Cartography at the TU Dresden, Alexander Dunkel, Eva Hauthal and Marc Löchner. Alex is a postdoc with a background in landscape and urban planning. The research of Eva, also a postdoc, aims at studying the emotions and sentiments of social media users. Marc is a PhD student and dedicates his doctorate to the privacy implications of evaluating social media data.

As social media data reflects real-life information from people's daily activities, it has become a popular source of information in research. This research usually does not focus on questions of (e.g.) advertising or user profiling, but on fundamental questions of society, particularly those that are connected with the increasing use of geo-social media, information spread, and associated environmental questions. During the two VGIscience phases, privacy became an increasingly relevant aspect for any project using social media data and VGI. Because people may not know that their data is being used for research purposes, the group has been working on methods to protect user privacy while still allowing researchers to analyze and visualize the data.

<video style="width:100%;height:auto;margin: 2em 0;" controls poster="https://downloads.vgiscience.org/projects/eva-privacy/poster.jpg" crossorigin="anonymous">
  <source src="https://downloads.vgiscience.org/projects/eva-privacy/av1.webm" type="video/webm">
  <source src="https://downloads.vgiscience.org/projects/eva-privacy/vp9.webm" type="video/webm">
  <source src="https://downloads.vgiscience.org/projects/eva-privacy/theora.ogg" type="video/ogg">
  <source src="https://downloads.vgiscience.org/projects/eva-privacy/avc.mp4" type="video/mp4">
  <track src="https://downloads.vgiscience.org/projects/eva-privacy/en.vtt" label="English" kind="subtitles" srclang="en" default>
  <track src="https://downloads.vgiscience.org/projects/eva-privacy/de.vtt" label="Deutsch" kind="subtitles" srclang="de">
  <p>Your browser does not support HTML5 video.</p>
</video>

Recent research results are released in the following publications:

{% bibliography --file 2022-12-21-eva-privacy.bib %}



[EVA-VGI]: /projects/eva-vgi-2.html
[Privacy Aspects]: /projects/privacy-aspects.html
