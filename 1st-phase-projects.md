---
layout: projects
project_phase: 1st_phase
redirect_from:
    - /completed-projects.html
---




# 1st phase Projects

These are the projects we worked on in the first phase from 2017 to 2019 of the VGIscience Priority Programme. You can find [the list of projects in the second phase here](2nd-phase-projects.html).

<!-- List of 1st phase projects -->
