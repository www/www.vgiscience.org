---
layout: projects
title: Projects
order: 3
project_phase: 2nd_phase
redirect_from: 
    - /projects/
    - /projects.html
---

# 2nd phase Projects

These are the projects we worked on in the second phase from 2020 to 2022 of the VGIscience Priority Programme. You can find [the list of projects in the first phase here](1st-phase-projects.html).

<!-- List of 2nd phase projects -->
