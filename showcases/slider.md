---
layout: image-slider-iframe
slides:
- image: https://gitlab.vgiscience.de/eva-vgi-2/eva-vgi-2/-/raw/master/assets/sunset-sunrise.png
  link: /projects/eva-vgi-2.html
  text: "Extraction and Visually Driven Analysis of VGI for Understanding People’s Behavior in Relation to Multi-Faceted Context"
- image: https://gitlab.vgiscience.de/landmarks/lm-uncertainty/-/raw/master/Fig_2_-_map_matching_task.PNG
  link: /projects/lm-uncertainty.html
  text: "The Effects of Landmark Uncertainty in VGI-based Maps: Approaches to Improve Wayfinding and Navigation Performance"
- image: /images/dvcha-showcase.png
  link: /projects/dvcha-2.html
  text: "Active Participation and Motivation of Professionalised Digital Volunteer Communities: Distributed Decision Making and its Impact on Disaster Management Organisations"
- image: https://gitlab.vgiscience.de/tovip/tovip/-/raw/master/images/preservation.png
  link: /projects/tovip.html
  text: "Improvement of Task-Oriented Visual Interpretation of VGI Point Data"
- image: https://gitlab.vgiscience.de/trajectories/trajectories-2/-/raw/master/MapMatchSemi.jpg
  link: /projects/trajectories-2.html
  text: "Dynamic and Customizable Exploitation of Trajectory Data"
- image: https://gitlab.vgiscience.de/expoaware/expoaware/-/raw/master/images/image9.png
  link: /projects/expoaware.html
  text: "Environmental volunteered geographic information for personal exposure awareness and healthy mobility behaviour"
- image: https://gitlab.vgiscience.de/vgireports/vgireports/-/raw/master/vgireports.jpg
  link: /projects/vgireports.html
  text: "Accessible Reporting of Spatiotemporal Geographic Information Leveraging Generated Text and Visualization"
- image: https://gitlab.vgiscience.de/birdtrace/birdtrace/-/raw/master/assets/animal-movement.png
  link: /projects/birdtrace.html
  text: "Uncertainty-Aware Enrichment of Animal Movement Trajectories by VGI"
- image: /images/vgi-routing-showcase.png
  link: /projects/vgi-routing.html
  text: "Inferring personalized multi-criteria routing models from sparse sets of voluntarily contributed trajectories"
- image: https://gitlab.vgiscience.de/ideal-vgi/ideal-vgi/-/raw/master/self-enhancement-maps.png
  link: /projects/ideal-vgi.html
  text: "Information Discovery from Big Earth Observation Data Archives by Learning from Volunteered Geographic Information"
- image: https://gitlab.vgiscience.de/va4vgi/va4vgi-2/-/raw/master/va4vgi-fig1.jpg
  link: /projects/va4vgi-2.html
  text: "Visual analysis of volunteered geographic information for interactive situation modeling and real-time event assessment"
- image: https://gitlab.vgiscience.de/worldkg/worldkg/-/raw/master/Images/OverviewWorldKG.png
  link: /projects/worldkg.html
  text: "World-Scale Completion of Geographic Knowledge"
- image: https://gitlab.vgiscience.de/privacy-aspects/privacy-aspects/-/raw/master/_images/sink-graphic.svg
  link: /projects/privacy-aspects.html
  text: "Privacy Aspects in Volunteered Geographic Information"
---
