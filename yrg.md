---
layout: yrg
title: Young Research
order: 5
redirect_from: /yrg/
---

# Young Research Groups

During the second phase of the VGIscience programme we formed so-called "Young
Research Groups" to promote self-organized cross-project collaboration between
PhD students involved with the VGIscience projects.

<!-- List of current projects -->
