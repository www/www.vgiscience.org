---
layout: home
---

<div style="border:0;width:100%;height:0;padding-bottom:56.25%;position:relative;margin-bottom:3em">
<iframe src="/showcases/slider.html" scrolling="no" style="border:0;position:absolute;width:100%;height:100%"></iframe>
</div>

## Volunteered Geographic Information Research Programme

Volunteered Geographic Information (VGI) has emerged in recent years as a new form of user-generated content.
VGI includes both the active generation of geospatial data, such as citizen science projects, and the passive collection of data through location-enabled mobile devices.
In addition, a growing number of sensors are capturing our environment in ever more detailed and dynamic ways.
The resulting VGI data can support various applications that address critical societal challenges, such as environmental and disaster management, health, transportation, and civic participation.

The interpretation and visualization of VGI is challenging due to the considerable heterogeneity of the underlying multi-source data and the social context.
In particular, the use of VGI is influenced by the specific characteristics of the underlying data, such as real-time availability, event-driven generation, and subjectivity, all with an implicit or explicit spatial reference.

This research programme was funded by German Research Foundation as [Priority Programme 1894](https://gepris.dfg.de/gepris/projekt/273827070). It ran from 2017 to 2022 and was divided into a [first phase](1st-phase-projects.html) and a [second phase](/2nd-phase-projects.html).

Of particular interest was the relationship between social behavior and technology in the collective collection, processing, and use of VGI. This includes, for example, the consideration of different collection and usage contexts, the evaluation of subjective information, and the assurance of privacy-aware data processing.

In order to promote advanced, self-organized, cross-project collaboration among the PhD students involved in the individual projects, the [Young Research Groups](yrg.html) initiative was implemented in the second phase.

Results of all the collaborative research within the priority programme have been made available through the [VGI-repository](repository.html), the sustainable public platform with access to benchmark data, VGI-related software tools, documentation, and [publications](publications).

---
