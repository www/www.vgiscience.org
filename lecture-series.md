---
layout: page
---

# VGIscience 2021 lecture series

In order to emphasize the collaborative progress of our current research work,
the *VGIscience 2021 lecture series* took place this year, each carried out
as a live online video conference.

Every other week on Thursday at 13:00, another VGIscience project group
introduced their research project and highlighted already achieved results of
their work in a 10-minute overview presentation. Following that a current
research question of the project was presented more in detail in a 15-minute
presentation with a chance for the audience to ask questions and discuss the
topic afterwards.

The 10-mintue overview presentations were recorded and made publicly available
afterwards here:

## Schedule

| Date       | Project                | Lectures                                                                                                                      |
| ---------- | ---------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| 2021-03-04 | [VGI-Routing]          | [Inferring personalized multi-criteria routing models from sparse sets of voluntarily contributed trajectories][01]           |
| 2021-03-18 | [COVMAP]               | [Continuation of Comprehensive Conjoint GPS, Sensor and Video Data][02]                                                       |
| 2021-04-01 | [EVA-VGI]              | [Geovisual analysis of VGI for understanding people’s behaviour in relation to multi-faceted context][03]                     |
| 2021-04-15 | [IDEAL-VGI]            | [Information Discovery from Big Earth Observation Data Archives by Learning from Volunteered Geographic Information][04]      |
| 2021-04-29 | [Trajectories]         | [Dynamic and Customizable Exploitation of Trajectory Data][05]                                                                |
| 2021-05-20 | [AQA]                  | [Algorithmic Quality Assurance: Theory and Practice][06]                                                                      |
| 2021-05-27 | [WorldKG]              | [World-Scale Completion of Geographic Knowledge][07]                                                                          |
| 2021-06-10 | [TOVIP]                | [Improvement of Task-Oriented Visual Interpretation of VGI Point Data][08]                                                    |
| 2021-06-24 | [VA4VGI]               | [Visual analysis of volunteered geographic information for interactive situation modeling and real-time event assessment][09] |
| 2021-07-08 | [ExpoAware]            | [Environmental volunteered geographic information for personal exposure awareness and healthy mobility behaviour][10]         |
| 2021-07-22 | [Privacy Aspects]      | [Privacy Aspects in Volunteered Geographic Information][11]                                                                   |
| 2021-08-05 | [DVCHA]                | [Distributed Decision Making and its Impact on Disaster Management Organisations][12]                                         |
| 2021-08-19 | [Landmark Uncertainty] | [The Effects of Landmark Uncertainty in VGI-based Maps: Approaches to Improve Wayfinding and Navigation Performance][13]      |
| 2021-09-02 | [BirdTrace]            | [Uncertainty-Aware Enrichment of Animal Movement Trajectories by VGI][14]                                                     |
| 2021-09-16 | [vgiReports]           | [Accessible Reporting of Spatiotemporal Geographic Information Leveraging Generated Text and Visualization][15]               |

[01]: https://downloads.vgiscience.org/lecture-series/vgi-routing/vgi-routing.html
[02]: https://downloads.vgiscience.org/lecture-series/covmap/covmap.html
[03]: https://downloads.vgiscience.org/lecture-series/eva-vgi/eva-vgi.html
[04]: https://downloads.vgiscience.org/lecture-series/ideal-vgi/ideal-vgi.html
[05]: https://downloads.vgiscience.org/lecture-series/trajectories/trajectories.html
[06]: https://downloads.vgiscience.org/lecture-series/aqa/aqa.html
[07]: https://downloads.vgiscience.org/lecture-series/worldkg/worldkg.html
[08]: https://downloads.vgiscience.org/lecture-series/tovip/tovip.html
[09]: https://downloads.vgiscience.org/lecture-series/va4vgi/va4vgi.html
[10]: https://downloads.vgiscience.org/lecture-series/expoaware/expoaware.html
[11]: https://downloads.vgiscience.org/lecture-series/privacy-aspects/privacy-aspects.html
[12]: https://downloads.vgiscience.org/lecture-series/dvcha/dvcha.html
[13]: https://downloads.vgiscience.org/lecture-series/lm-uncertainty/lm-uncertainty.html
[14]: https://downloads.vgiscience.org/lecture-series/birdtrace/birdtrace.html
[15]: https://downloads.vgiscience.org/lecture-series/vgireports/vgireports.html

[VGI-Routing]: https://www.vgiscience.org/projects/vgi-routing.html
[COVMAP]: https://www.vgiscience.org/projects/covmap-2.html
[EVA-VGI]: https://www.vgiscience.org/projects/eva-vgi-2.html
[IDEAL-VGI]: https://www.vgiscience.org/projects/ideal-vgi.html
[Trajectories]: https://www.vgiscience.org/projects/trajectories-2.html
[AQA]: https://www.vgiscience.org/projects/aqa.html
[WorldKG]: https://www.vgiscience.org/projects/worldkg.html
[TOVIP]: https://www.vgiscience.org/projects/tovip.html
[VA4VGI]: https://www.vgiscience.org/projects/va4vgi-2.html
[ExpoAware]: https://www.vgiscience.org/projects/expoaware.html
[Privacy Aspects]: https://www.vgiscience.org/projects/privacy-aspects.html
[DVCHA]: https://www.vgiscience.org/projects/dvcha-2.html
[Landmark Uncertainty]: https://www.vgiscience.org/projects/lm-uncertainty.html
[BirdTrace]: https://www.vgiscience.org/projects/birdtrace.html
[vgiReports]: https://www.vgiscience.org/projects/vgireports.html

<style>
    table {
        width: 100%;
    }
    th {
        text-align: left;
    }
    table td {
        vertical-align: baseline;
        padding: 1em 1em 0 0;
    }
    table td:nth-child(3) {
        white-space: break-spaces;
    }
</style>
